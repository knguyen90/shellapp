﻿using System;
using System.Linq;
using Xamarin.Forms;

namespace OORETA.Views.Behaviors
{
    public class ViewTappedBehavior : Behavior<View>
    {
        protected override void OnAttachedTo(View bindable)
        {
            var exists = bindable.GestureRecognizers.FirstOrDefault() as TapGestureRecognizer;

            if (exists != null)
                exists.Tapped += View_Tapped;

            base.OnAttachedTo(bindable);
        }

        protected override void OnDetachingFrom(View bindable)
        {
            var exists = bindable.GestureRecognizers.FirstOrDefault() as TapGestureRecognizer;

            if (exists != null)
                exists.Tapped -= View_Tapped;

            base.OnDetachingFrom(bindable);
        }

        bool _isAnimating = false;

        void View_Tapped(object sender, EventArgs e)
        {
            if (_isAnimating)
                return;

            _isAnimating = true;

            var view = (View)sender;

            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    Color oldColor = default(Color);
                    var labelView = sender as Label;
                    if (labelView != null)
                    {
                        oldColor = labelView.TextColor;
                        labelView.TextColor = Color.Black;
                    }

                    await view.FadeTo(0.5d, 80, Easing.SinIn);
                    await view.FadeTo(1d, 80, Easing.SinIn);

                    if (labelView != null)
                    {
                        labelView.TextColor = oldColor;
                    }
                }
                finally
                {
                    _isAnimating = false;
                }
            });
        }
    }
}
