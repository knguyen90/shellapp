﻿using System;
using System.Collections.Generic;
using OORETA.ViewModels;
using Xamarin.Forms;

namespace OORETA.Views.Pages
{
    public partial class FindJobsPage : ContentPage
    {
        public FindJobsPage()
        {
            InitializeComponent();
            this.BindingContext = new FindJobsVM();
            NavigationPage.SetBackButtonTitle(this, " ");
        }
    }
}
