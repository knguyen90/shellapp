﻿using System;
using System.Collections.Generic;
using OORETA.ViewModels;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace OORETA.Views.Pages
{
    public partial class TodayPage : ContentPage
    {
        public TodayPage()
        {
            InitializeComponent();
            this.BindingContext = new TodayHoroscopeVM();
        }
    }
}
