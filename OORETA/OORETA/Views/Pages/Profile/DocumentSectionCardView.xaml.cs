﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.PancakeView;

namespace OORETA.Views.Pages.Profile
{
    public partial class DocumentSectionCardView : PancakeView
    {
        public string Title { set {
                this.titleLabel.Text = value;
            }
        }

        public string IconString
        {
            set
            {
                this.titleFontIcon.Glyph = value;
            }
        }

        public Color IconBackgroundColor
        {
            set
            {
                this.titleIconBackground.BackgroundColor = value;
            }
        }

        public Color IconColor
        {
            set
            {
                this.titleFontIcon.Color = value;
            }
        }

        public DocumentSectionCardView()
        {
            InitializeComponent();
        }
    }
}
