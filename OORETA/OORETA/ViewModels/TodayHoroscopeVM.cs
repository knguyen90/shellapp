﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using OORETA.Models;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace OORETA.ViewModels
{
    public class TodayHoroscopeVM : INotifyPropertyChanged
    {
        private List<TodaySignHeader> _headers;
        public List<TodaySignHeader> Headers {
            set => _headers = value;
            get => _headers;
        }

        public ICommand SelectItemCommand { get; private set; }

        private TodaySignHeader _selectedItem;
        public TodaySignHeader SelectedItem {
            get => _selectedItem;
            set {
                if (_selectedItem != value) {
                    _selectedItem = value;
                    OnPropertyChanged(nameof(SelectedItem));
                }
            }
        }

        public TodayHoroscopeVM()
        {
            _headers = new List<TodaySignHeader>()
            {
                new TodaySignHeader(ZodiacSign.Aries),
                new TodaySignHeader(ZodiacSign.Leo),
                new TodaySignHeader(ZodiacSign.Sagittarius),

                
                new TodaySignHeader(ZodiacSign.Capricorn),
                new TodaySignHeader(ZodiacSign.Virgo),
                new TodaySignHeader(ZodiacSign.Taurus),
                
                
                new TodaySignHeader(ZodiacSign.Gemini),
                new TodaySignHeader(ZodiacSign.Cancer),
                new TodaySignHeader(ZodiacSign.Libra),


                new TodaySignHeader(ZodiacSign.Scorpio),
                new TodaySignHeader(ZodiacSign.Aquarius),
                new TodaySignHeader(ZodiacSign.Pisces)
            };
            SelectItemCommand = new Command(SelectItemTask);
        }

        private void SelectItemTask() {
            if (SelectedItem == null)
                return;
            SelectedItem = null;

            var state = Shell.Current.CurrentState;
            MainThread.BeginInvokeOnMainThread(async () =>
            {
                await Shell.Current.GoToAsync($"{state.Location}/signTodayDetails", true);
            });
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged([CallerMemberName]string propertyName = "") =>
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
