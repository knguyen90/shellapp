﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace OORETA.ViewModels.ProfileSections
{
    public class IdCardVM : BaseVM
    {
        private IEnumerable<string> _photoAttachments;
        public IEnumerable<string> PhotoAttachments {
            get => _photoAttachments;
            set => _photoAttachments = value;
        } 

        public IdCardVM()
        {
            _photoAttachments = new string[] {
                "1", "2"
            };
        }
    }
}
