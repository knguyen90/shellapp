﻿using System;
using System.Collections.Generic;
using OORETA.Models;

namespace OORETA.ViewModels
{
    public class FindJobsVM
    {
        private List<JobHeader> _currentJobs;
        public List<JobHeader> CurrentJobs
        {
            set => _currentJobs = value;
            get => _currentJobs;
        }
        public FindJobsVM()
        {
            _currentJobs = new List<JobHeader>()
            {
                new JobHeader(),
                new JobHeader(),
                new JobHeader(),
                new JobHeader(),
                new JobHeader(),
                new JobHeader(),
                new JobHeader(),
                new JobHeader(),
                new JobHeader(),
            };
        }
    }
}
