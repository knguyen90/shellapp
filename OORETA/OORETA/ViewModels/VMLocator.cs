﻿using System;
using OORETA.ViewModels.ProfileSections;

namespace OORETA.ViewModels
{
    public class VMLocator
    {
        public ProfileVM Profile => new ProfileVM();
        public IdCardVM IdCard => new IdCardVM();
    }
}
