﻿using System.Windows.Input;
using OORETA.Views.Pages.Profile.Sections;
using Xamarin.Forms;

namespace OORETA.ViewModels
{
    public class ProfileVM : BaseVM
    {
        public ICommand GoToIdCardPageCommand { get; }

        public ProfileVM()
        {
            GoToIdCardPageCommand = new Command(async () => {
                var idCardPage = new IdCardPage();
                Shell.SetTabBarIsVisible(idCardPage, false);
                await Application.Current.MainPage.Navigation.PushAsync(idCardPage);
            });
        }
    }
}
