﻿using System;
using System.ComponentModel;
using Xamarin.Forms;

namespace OORETA.Models
{
    public enum ZodiacSign {
        Aries, Gemini, Cancer, Leo, Virgo, Libra, Scorpio, Sagittarius, Capricorn, Aquarius, Pisces, Taurus
    }

    public class TodaySignHeader : INotifyPropertyChanged
    {
        private ZodiacSign _sign;

        public string SignName {
            get => ZodiacSignName();
        }

        public Color BackgroundColor {
            get => BackgroundColorBySign();
        }

        public Color TextColor
        {
            get => TextColorBySign();
        }

        public TodaySignHeader(ZodiacSign sign)
        {
            _sign = sign;
        }

        private string ZodiacSignName() {
            switch (_sign) {
                case ZodiacSign.Taurus:
                    return "Taurus";
                case ZodiacSign.Aries:
                    return "Aries";
                case ZodiacSign.Gemini:
                    return "Gemini";
                case ZodiacSign.Cancer:
                    return "Cancer";
                case ZodiacSign.Libra:
                    return "Libra";
                case ZodiacSign.Leo:
                    return "Leo";
                case ZodiacSign.Virgo:
                    return "Virgo";
                case ZodiacSign.Scorpio:
                    return "Scorpio";
                case ZodiacSign.Sagittarius:
                    return "Sagittarius";
                case ZodiacSign.Capricorn:
                    return "Capricorn";
                case ZodiacSign.Aquarius:
                    return "Aquarius";
                case ZodiacSign.Pisces:
                    return "Pisces";
            }
            return string.Empty;
        }

        private Color BackgroundColorBySign()
        {
            switch (_sign)
            {
                case ZodiacSign.Aries:
                    return Color.FromHex("#FDEDEC");
                case ZodiacSign.Taurus:
                    return Color.FromHex("#FDF2E9");
                case ZodiacSign.Gemini:
                    return Color.FromHex("#FEF9E7");
                case ZodiacSign.Cancer:
                    return Color.FromHex("#E8F6F3");
                case ZodiacSign.Libra:
                    return Color.FromHex("#E8F8F5");
                case ZodiacSign.Leo:
                    return Color.FromHex("#FEF5E7");
                case ZodiacSign.Virgo:
                    return Color.FromHex("#EAFAF1");
                case ZodiacSign.Scorpio:
                    return Color.FromHex("#F3E5F5");
                case ZodiacSign.Sagittarius:
                    return Color.FromHex("#FBEEE6");
                case ZodiacSign.Capricorn:
                    return Color.FromHex("#F5EEF8");
                case ZodiacSign.Aquarius:
                    return Color.FromHex("#EBF5FB");
                case ZodiacSign.Pisces:
                    return Color.FromHex("#EAF2F8");
            }
            return Color.Black;
        }

        private Color TextColorBySign()
        {
            switch (_sign)
            {
                case ZodiacSign.Aries:
                    return Color.FromHex("#E74C3C");
                case ZodiacSign.Taurus:
                    return Color.FromHex("#E67E22");
                case ZodiacSign.Gemini:
                    return Color.FromHex("#F1C40F");
                case ZodiacSign.Cancer:
                    return Color.FromHex("#16A085");
                case ZodiacSign.Libra:
                    return Color.FromHex("#1ABC9C");
                case ZodiacSign.Leo:
                    return Color.FromHex("#F39C12");
                case ZodiacSign.Virgo:
                    return Color.FromHex("#58D68D");
                case ZodiacSign.Scorpio:
                    return Color.FromHex("#9C27B0");
                case ZodiacSign.Sagittarius:
                    return Color.FromHex("#D35400");
                case ZodiacSign.Capricorn:
                    return Color.FromHex("#9B59B6");
                case ZodiacSign.Aquarius:
                    return Color.FromHex("#3498DB");
                case ZodiacSign.Pisces:
                    return Color.FromHex("#2980B9");
            }
            return Color.Black;
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
