﻿using System;
using System.Collections.Generic;
using OORETA.Views.Pages;
using Xamarin.Forms;

namespace OORETA
{
    public partial class AppShellPage : Shell
    {
        public AppShellPage()
        {
            InitializeComponent();
            Routing.RegisterRoute("signTodayDetails", typeof(TodaySignDetailsPage));
            NavigationPage.SetBackButtonTitle(this, string.Empty);
        }
    }
}
